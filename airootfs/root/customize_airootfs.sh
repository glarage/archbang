#!/usr/bin/env bash
# Configure live iso
#
set -e -u -x
shopt -s extglob

# Set locales
sed -i 's/#\(fr_FR\.UTF-8\)/\1/' /etc/locale.gen
locale-gen

# Allow Parallel Downloads in pacman 
sed -i -e 's|[#]NoExtract   =|NoExtract   = usr/share/gtk-doc/html/* usr/share/doc/*\nNoExtract   = usr/share/locale/[a-z]*/*\nNoExtract   = !usr/share/locale/fr/*\nNoExtract   = !usr/share/locale/fr_FR/*\nNoExtract   = usr/lib/firefox/browser/features/screenshots@mozilla.org.xpi\nNoExtract   = usr/share/dbus-1/services/org.a11y.*\nNoExtract   = usr/share/help/* !usr/share/help/fr*|g' /etc/pacman.conf
sed -i -e 's|[#]Color|Color|g' /etc/pacman.conf
sed -i -e 's|[#]VerbosePkgLists|VerbosePkgLists|g' /etc/pacman.conf
sed -i "s/^#Parallel/Parallel/g" /etc/pacman.conf
 

# Sudo to allow no password
sed -i 's/# %wheel ALL=(ALL:ALL) NOPASSWD: ALL/%wheel ALL=(ALL:ALL) NOPASSWD: ALL/g' /etc/sudoers
chown -c root:root /etc/sudoers
chmod -c 0440 /etc/sudoers

# Hostname
echo "archbang" > /etc/hostname

# Vconsole
echo "KEYMAP=fr" > /etc/vconsole.conf
echo "FONT=" >> /etc/vconsole.conf

# Locale
echo "LANG=fr_FR.UTF-8" > /etc/locale.conf
echo "LC_COLLATE=C" >> /etc/locale.conf

# Set clock to UTC
hwclock --systohc --utc

# Timezone
ln -sf /usr/share/zoneinfo/Europe/Paris /etc/localtime

# Add live user
useradd -m -p "" -G "wheel" -s /bin/bash ablive
chown ablive /home/ablive

# Remove /etc/skel home config files 
#rm -r /etc/skel/ 
#mkdir /etc/skel 

# remove desktop files
path="/usr/share/applications"

desk_file='avahi-discover bvnc bssh volumeicon gparted conky tint2 pcmanfm-desktop-pref qv4l2 qvidcap'

for rem in ${desk_file}
do
   mv ${path}/${rem}.desktop ${path}/${rem}.hide
done

# Start required systemd services
systemctl enable {pacman-init,NetworkManager}.service -f

#systemctl set-default multi-user.target
systemctl set-default graphical.target

# Revert from archiso preset to default preset
cp -rf "/usr/share/mkinitcpio/hook.preset" "/etc/mkinitcpio.d/linux.preset"
sed -i 's?%PKGBASE%?linux?' "/etc/mkinitcpio.d/linux.preset"

